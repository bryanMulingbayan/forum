<?php

namespace App\Filters;
use Illuminate\Http\Request;

abstract class Filters 
{
    protected $request, $queryBuilder;
    protected $filters = [];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function apply($queryBuilder)
    {
        $this->queryBuilder = $queryBuilder;
        
        foreach($this->getFilters() as $filter => $value) {
            if(method_exists($this, $filter)) {

                $this->$filter($value);
            }
        }

        return $this->queryBuilder;
    }

    
    public function getFilters()
    {
        return $this->request->only($this->filters);
    }
}
