<?php

namespace App\Filters;
use App\User;

class ThreadFilters extends Filters
{
    protected $filters = ['by', 'popular', 'unanswered'];

    protected function by( $username)
    {
        $user = User::where('name', $username)->firstOrFail();

        return $this->queryBuilder->where('user_id', $user->id);
    }

    protected function popular()
    {
        return $this->queryBuilder->orderBy('replies_count', 'desc');
    }

    protected function unanswered()
    {
        return $this->queryBuilder->where('replies_count', 0);
    }
}
