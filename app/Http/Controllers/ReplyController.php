<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Thread;
use App\Reply;
use Illuminate\Support\Facades\Gate;
use App\Http\Requests\CreatePostForm;
use App\Rules\SpamFree;
use App\Notifications\YouWereMentioned;
use App\User;


class ReplyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index']);
    }

    /**
     * Get all replies under specific thread.
     *
     * @param String $channelId
     * @param Thread $thread
     * @return replies
     */
    public function index($channelId, Thread $thread)
    {
        return $thread->replies()->paginate(10); 
    }

    /**
     * Create new Reply.
     *
     * @param Request $request
     * @param Strring $channelId
     * @param Thread $thread
     * @param CreatePostForm $form
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function store(Request $request, $channelId, Thread $thread, CreatePostForm $form)
    {
        return $thread->addReply([
            'body' => $request->body, 
            'user_id' => Auth::id()
        ])->load('owner');
    }

    /**
     * Delete reply.
     *
     * @param Reply $reply
     */
    public function destroy(Reply $reply)
    {
        $this->authorize('update', $reply);

        $reply->delete();

        if(request()->expectsJson()) {
            return response(['status' => 'Your reply has been deleted']);
        }

        return back();
    }

    /**
     * Update specific reply.
     *
     * @param Request $request
     * @param Reply $reply
     */
    public function update(Request $request, Reply $reply)
    {
        $this->authorize('update', $reply);

        $request->validate([
            'body' => new SpamFree,
        ]);

        $reply->update([ 'body' => request('body') ]);

        return response(['status' => 'Your reply has been deleted']);
    }
}