<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use App\Notifications\ThreadWasUpdated;
use App\Events\ThreadHasNewReply;
use App\Spam;

class Thread extends Model
{
    use RecordsActivity;

    protected $guarded = [];
    protected $with = ['creator', 'channel'];
    protected $appends = ['isSubscribedTo'];

    protected static function boot()
    {
        parent::boot();

        static::deleting(function ($thread) {
            $thread->replies->each->delete();
        });
    }

    /**
     * Get path of the thread.
     */
    public function path()
    {
        return '/threads/' . $this->channel->slug . '/' . $this->slug;
    }

    /**
     * Get replies under specific thread.
     */
    public function replies()
    {
        return $this->hasMany(Reply::class);
    }

    /**
     * Get user who created this thread.
     */
    public function creator()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Add reply to this thread.
     * @param Reply $reply
     */
    public function addReply($reply)
    {
        $reply = $this->replies()->create($reply);

        event(new ThreadHasNewReply($this, $reply));

        // $this->notifySubscribers($reply);

        return $reply;
    }

    public function notifySubscribers($reply)
    {
        $this->subscriptions
            ->where('user_id', '!=', $reply->user_id)
            ->each
            ->notify($reply);
    }

    /**
     * Get channel where this thread belongs.
     */
    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    /**
     * Add filters to list of threads.
     *
     * @param QueryBuilder $query
     * @param Filters $filters
     * @return QueryBuilder $filters
     */
    public function scopeFilter($query, $filters)
    {
        return $filters->apply($query);
    }

    public function unsubscribe($userId = null)
    {
        $this->subscriptions()
            ->where('user_id', $userId ? : Auth::id())
            ->delete();
    }

    public function subscribe($userId = null)
    {
        $this->subscriptions()->create([
            'user_id' => $userId ? : Auth::id()
        ]);

        return $this;
    }

    public function subscriptions()
    {
        return $this->hasMany(ThreadSubscription::class);
    }

    public function getIsSubscribedToAttribute()
    {
        return $this->subscriptions()
            ->where('user_id', Auth::id())
            ->exists();
    }

    public function hasUpdatesFor($user)
    {
        $key = $user->visitedThreadCacheKey($this);

        return $this->updated_at > cache($key);
    }

    public function getRouteKeyName()
    {
        return 'slug';
    }
}
