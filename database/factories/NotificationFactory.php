<?php

use Faker\Generator as Faker;
use Illuminate\Support\Facades\Auth;

$factory->define(\Illuminate\Notifications\DatabaseNotification::class, function (Faker $faker) {
    return [
        'id' => \Ramsey\Uuid\Uuid::uuid4()->toString(),
        'type' => 'App\Notifications\ThreadWasUpdated',
        'notifiable_id' => function ()
            {
                return Auth::id() ?: factory('App\User')->create()->id;
            },
        'notifiable_type' => 'App\User',
        'data' => ['foo' => 'bar'] 
    ];
});
