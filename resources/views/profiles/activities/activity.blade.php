<div class="card" style="margin: 10px 0px">
    <div class="card-header">
        <div class="d-flex justify-content-between">
            {{ $heading }}
        </div>
    </div>

    <div class="card-body">
        <article>
            {{ $body }}
        </article>
    </div>
</div>