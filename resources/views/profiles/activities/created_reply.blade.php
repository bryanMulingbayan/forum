@component('profiles.activities.activity')
    @slot('heading')
    <div>
        {{ $profileUser->name }} replied to a 
        <a href="{{ $activity->subject->thread->path() }}">
                "{{ $activity->subject->thread->title }}" 
        </a>
    </div>
    @endslot

    @slot('body')
        {{ $activity->subject->body }}
    @endslot

@endcomponent