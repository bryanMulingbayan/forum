@component('profiles.activities.activity')
    @slot('heading')
    <div>
        {{ $profileUser->name }} published 
        <a href="{{ $activity->subject->path() }}">
            "{{ $activity->subject->title }}" 
        </a>
    </div>
    @endslot

    @slot('body')
        {{ $activity->subject->body }}
    @endslot

@endcomponent