@forelse ($threads as $thread) 
    <div class="card" style="margin: 10px 0px">
        <div class="card-header">
            <div class="d-flex justify-content-between">
                <div class="flex">
                    <h4>
                        <a href="{{ $thread->path() }}"> 
                             @if( Auth::check() && $thread->hasUpdatesFor(Auth::user()))
                                <strong>{{ $thread->title }} </strong>
                            @else
                                {{ $thread->title }} 
                            @endif
                         </a>
                    </h4>
                    <h5>
                         Posted by: 
                            <a href="{{ route('profile', $thread->creator) }}">
                                {{ $thread->creator->name }}
                            </a>
                    </h5>
                </div>
                            
                <a href="{{ $thread->path() }}">
                    <small>
                        <strong>
                            {{ $thread->replies_count }} {{ str_plural('comment', $thread->replies_count) }}.
                        </strong>
                    </small>
                </a>
            </div>  
        </div>
        <div class="card-body">
            <div class="body"> {{ $thread->body }} </div>
        </div>

        <div class="card-footer">
            {{ $thread->visits }} Visits
        </div>
    </div>
@empty
    <p>There are no relevant results at this time.</p>
@endforelse