@extends('layouts.app')


@section('header')
    <link href="{{ asset('css/jquery.atwho.css') }}" rel="stylesheet">
@endsection

@section('content')
<thread-view inline-template :initial-replies-count="{{ $thread->replies_count }}">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="card mb-2">
                    <div class="card-header">
                        <div class="d-flex justify-content-between">
                            <div>
                                <img src="{{ $thread->creator->avatar() }}" width="25" height="25" class="mr-1">
                                <a href="{{ route('profile', $thread->creator) }}"> {{ $thread->creator->name }} </a> posted {{ $thread->title }} .. 
                            </div>
                            <div>
                                @if (auth()->check())
                                    @can('update', $thread)
                                        <form method="POST" action="{{ $thread->path() }}">
                                            {{ csrf_field() }}
                                            {{ method_field('DELETE') }}
                                            <button type="submit" class="btn btn-link btn-sm">
                                                Delete Thread
                                            </button>
                                        </form>
                                    @endcan
                                @endif
                            </div>
                        </div> 
                    </div>

                    <div class="card-body">
                        <article>
                            <div class="body"> {{ $thread->body }} </div>
                        </article>
                    </div>
                </div>

                <replies @added="repliesCount++" @removed="repliesCount--"></replies>

            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body">
                        <p>
                            This thread was published {{ $thread->created_at->diffForHumans() }} by
                            <a href="#">{{ $thread->creator->name }}</a> and currently has
                            <span v-text="repliesCount"></span>
                            {{ str_plural('comment', $thread->replies_count) }}.
                        </p>
                        <p>
                            <subscribe-button :active="{{ json_encode($thread->isSubscribedTo) }}"></subscribe-button>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</thread-view>
@endsection