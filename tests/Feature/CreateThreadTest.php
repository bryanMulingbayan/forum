<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use App\Activity;
use Carbon\Carbon;

class CreateThreadTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    function authenticated_user_must_first_verify_their_email_before_creating_thread()
    {
        $this->publishThreadWithoutVerified()
            ->assertSee('Your email address is not verified.');
    }

    /** @test */
    function an_authenticate_user_can_create_new_forum_threads()
    {
        $this->signIn();

        Auth::user()->update(['email_verified_at' => Carbon::now()]);

        $thread = make('App\Thread');
        
        $response = $this->post('/threads', $thread->toArray());

        $this->get($response->headers->get('Location'))
            ->assertSee($thread->title)
            ->assertSee($thread->body);
    }

    /** @test */
    function guess_may_not_create_threads()
    {
        $this->withExceptionHandling();

        $this->post('/threads')
            ->assertRedirect('/email/verify');

        $this->get('/threads/create')
            ->assertRedirect('/email/verify');
    }

    /** @test */
    function a_thread_requires_a_title()
    {
        $this->publishThread(['title' => null])
            ->assertStatus(422);
    }

    /** @test */
    function a_thread_requires_a_body()
    {
        $this->publishThread(['body' => null])
            ->assertStatus(422);
    }

    /** @test */
    function a_thread_requires_a_valid_channel()
    {
        $this->publishThread(['channel_id' => null])
            ->assertStatus(422);

        $this->publishThread(['channel_id' => 999])
            ->assertStatus(422);
    }

    /** @test */
    function authorized_user_can_delete_thread()
    {
        $this->signIn();
        $thread = create('App\Thread', ['user_id' => Auth::id()]);
        $reply  = create('App\Reply', ['thread_id' => $thread->id]);

        $response = $this->json('DELETE', $thread->path());

        $response->assertStatus(204);

        $this->assertDatabaseMissing('threads', ['id' => $thread->id]);
        $this->assertDatabaseMissing('replies', ['id' => $reply->id]);
        $this->assertEquals(0, Activity::count());
    }

    /** @test */
    function unauthorized_users_may_not_delete_threads()
    {
        $this->withExceptionHandling();

        $thread = create('App\Thread');

        $this->delete($thread->path())
            ->assertRedirect('/login');

        $this->signIn();

        $this->delete($thread->path())
            ->assertStatus(403);
    }

    public function publishThread($overrides = [])
    {
        $this->withExceptionHandling()->signIn();

        Auth::user()->update(['email_verified_at' => Carbon::now()]);

        $thread = make('App\Thread', $overrides);

        return $this->json('post','/threads', $thread->toArray());
    }

    public function publishThreadWithoutVerified($overrides = [])
    {
        $this->withExceptionHandling()->signIn();

        $thread = make('App\Thread', $overrides);

        return $this->json('post','/threads', $thread->toArray());
    }


}
