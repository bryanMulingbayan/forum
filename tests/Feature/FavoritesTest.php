<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;

class FavoritesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_authenticated_user_can_favorite_any_reply()
    {   
        $this->signIn();

        $reply = create('App\Reply');
        
        $this->post('/replies/' . $reply->id . '/favorites');

        $this->assertCount(1, $reply->favorites);
    }

    /** @test  */
    public function guess_cannot_favorite_anything()
    {   
        $reply = create('App\Reply');

        $this->withExceptionHandling()
            ->post('/replies/' . $reply->id . '/favorites')
            ->assertRedirect('login');
    }

    /** @test */
    public function an_authenticate_user_may_favorite_reply_once()
    {
        $this->signIn();

        $reply = create('App\Reply');

        try {
            $this->post('/replies/' . $reply->id . '/favorites');
            $this->post('/replies/' . $reply->id . '/favorites');
        } catch (\Exception $e) {
            $this->fail('Din not expect two insert the same record twice');
        }
        

        $this->assertCount(1, $reply->favorites);
    }

    /** @test */
    public function an_authenticated_user_can_unfavorite_any_reply()
    {   
        $this->signIn();
    
        $reply = create('App\Reply');
            
        $reply->favorite(Auth::id());

        $this->delete('/replies/' . $reply->id . '/favorites');
        $this->assertCount(0, $reply->favorites);

    }
}
