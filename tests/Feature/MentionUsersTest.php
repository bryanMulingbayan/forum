<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MentionUsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function mentioned_user_on_reply_can_be_notified()
    {
        $userJohn = create('App\User', ['name' => 'JohnDoe']);

        $this->signIn($userJohn);

        $userJane = create('App\User', ['name' => 'JaneDoe']);

        $thread = create('App\Thread');

        $reply = make('App\Reply', [
            'body' => '@JaneDoe look at this one'
        ]);

        $this->post($thread->path() . '/replies', $reply->toArray());

        $this->assertCount(1, $userJane->notifications);
    }

    /** @test */
    public function it_can_fetch_users_with_the_given_characters()
    {
        create('App\User', ['name' => 'johndoe']);
        create('App\User', ['name' => 'johndodo']);
        create('App\User', ['name' => 'janedoe']);

        $results = $this->json('GET', '/api/users', ['name' => 'john']);

        $this->assertCount(2, $results->json());
    }
}
