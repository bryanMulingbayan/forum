<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\DatabaseNotification;
use App\ThreadSubscription;

class NotificationsTest extends TestCase
{

    use Refreshdatabase;

    public function setUp()
    {
        parent::setUp();

        $this->signIn();
    }

    /** @test */
    public function a_notification_is_prepared_when_a_subscribed_thread_receives_a_new_reply_that_is_not_by_the_current_user()
    {
        $thread = create('App\Thread')->subscribe();

        $this->assertCount(1, $thread->fresh()->subscriptions);

        $this->assertCount(0, Auth::user()->notifications);

        $thread->addReply([
            'user_id' => Auth::id(),
            'body'    => 'Some Replies'
        ]);

        $this->assertCount(0, Auth::user()->fresh()->notifications);

        $thread->addReply([
            'user_id' => create('App\User')->id,
            'body'    => 'Some Replies'
        ]);

        $this->assertCount(1, Auth::user()->fresh()->notifications);
    }

    /** @test */
    public function a_user_can_mark_notification_as_read()
    {
        create(DatabaseNotification::class);
 
        tap(Auth::user(), function ($user) {

            $this->assertCount(1, $user->unreadNotifications);
    
            $this->delete('/profiles/' . $user->name .'/notifications/' . $user->unreadNotifications->first()->id);
    
            $this->assertCount(0, $user->fresh()->unreadNotifications);
        });
    }

    /** @test */
    function a_user_can_fetch_their_unread_notification()
    {
        create(DatabaseNotification::class);

        $this->assertCount(
            1, 
            $this->getJson('/profiles/' . Auth::user()->name .'/notifications/')->json()
        );
    }
}
