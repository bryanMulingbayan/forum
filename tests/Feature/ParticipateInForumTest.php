<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;

class ParticipateInForumTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function an_authenticated_user_may_participate_in_threads_forum()
    {
        $this->signIn();

        $thread = create('App\Thread');
        $reply  = make('App\Reply');

        $this->post($thread->path() . '/replies', $reply->toArray());

        $this->assertDatabaseHas('replies', ['body' => $reply->body]);

        $this->assertEquals(1, $thread->fresh()->replies_count);
    }

    /** @test */
    public function an_unauthenticated_user_may_not_post_a_reply()
    {
        $thread = create('App\Thread');

        $this->withExceptionHandling()
            ->post($thread->path() . '/replies', [])
            ->assertRedirect('/login');
    }

    /** @test */
    function a_reply_requires_a_body()
    {
        $this->withExceptionHandling()->signIn();

        $thread = create('App\Thread');
        $reply  = make('App\Reply', ['body' => null]);

        $this->json('post', $thread->path() . '/replies', $reply->toArray())
            ->assertStatus(422);
    }

    /** @test */
    function unauthorized_user_cannot_delete_replies()
    {
        $this->withExceptionHandling();
        $reply = create('App\Reply');

        $this->delete("/replies/{$reply->id}")
            ->assertRedirect('/login');

        $this->signIn()
            ->delete("/replies/{$reply->id}")
            ->assertStatus(403);
    }

    /** @test */
    function authorized_user_can_delete_replies()
    {
        $this->signIn();

        $reply = create('App\Reply', ['user_id' => Auth::id()]);

        $this->delete("/replies/{$reply->id}")
            ->assertStatus(302);

        $this->assertDatabaseMissing('replies', ['id' => $reply->id]);

        $this->assertEquals(0, $reply->thread->fresh()->replies_count);
    }

    /** @test */
    function authorized_user_can_update_replies()
    {
        $updatedMessage = "my Updated Message";
        $this->signIn();

        $reply = create('App\Reply', ['user_id' => Auth::id()]);

        $this->patch("/replies/{$reply->id}", [
            'body' => $updatedMessage
        ]);

        $this->assertDatabaseHas('replies', [
            'id'   => $reply->id,
            'body' => $updatedMessage
            ]);
    }

    /** @test */
    function unauthorized_user_cannot_update_replies()
    {
        $this->withExceptionHandling();
        $reply = create('App\Reply');
    
        $this->patch("/replies/{$reply->id}")
            ->assertRedirect('/login');
    
        $this->signIn()
            ->patch("/replies/{$reply->id}")
            ->assertStatus(403);
    }

    /** @test */
    function replies_that_contain_spam_may_not_be_created()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $thread = create('App\Thread');

        $reply  = make('App\Reply', [
            'body' => 'Yahoo Customer Support'
            ]);

        $this->json('post', $thread->path() . '/replies', $reply->toArray())
            ->assertStatus(422);
    }

    /** @test */
    function a_user_can_only_leave_a_reply_once_a_minute()
    {
        $this->withExceptionHandling();

        $this->signIn();

        $thread = create('App\Thread');

        $reply  = make('App\Reply', [
                'body' => 'Simple Replyy'
            ]);

        $this->post($thread->path() . '/replies', $reply->toArray())
            ->assertStatus(201);

        $this->post($thread->path() . '/replies', $reply->toArray())
            ->assertStatus(429);
    }
}