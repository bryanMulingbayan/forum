<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;

class ProfilesTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_has_profile()
    {
        $user = create('App\User'); 
        
        $this->get("/profiles/{$user->name}")
            ->assertSee($user->name);
    }

    /** @test */
    public function profile_display_all_threads_created_by_user()
    {
        $this->signIn();

        $thread = create('App\Thread', ['user_id' => Auth::id() ]);

        $this->get("/profiles/" . Auth::user()->name)
            ->assertSee($thread->title)
            ->assertSee($thread->body);
    }
}
