<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Illuminate\Notifications\Notification;
use App\Notifications\ThreadWasUpdated;
use Carbon\Carbon;
use Illuminate\Support\Facades\Redis;

class ThreadTest extends TestCase
{
    use RefreshDatabase;
    protected $thread;

    public function setUp()
    {
        parent::setUp();
        $this->thread = create('App\Thread');
    }

    /** @test */
    function it_has_replies()
    {
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $this->thread->replies);
    }

    /** @test */
    function it_has_a_creator()
    {
        $this->assertInstanceOf('App\User', $this->thread->creator);
    }

    /** @test */
    function it_can_add_a_reply()
    {
        $this->thread->addReply([
            'body'  => 'Foobar',
            'user_id' => 1
        ]);

        $this->assertCount(1, $this->thread->replies);
    }

    /** @test */
    function it_belongs_to_a_channer()
    {
        $this->assertInstanceOf('App\Channel', $this->thread->channel);
    }

    /** @test */
    function it_can_make_string_path()
    {
        $this->assertEquals('/threads/' . $this->thread->channel->slug . '/' . $this->thread->slug, $this->thread->path());
    }

    /** @test */
    public function it_can_be_subscribed_to()
    {
        $thread = create('App\Thread');

        $thread->subscribe($userId = 1);

        $this->assertEquals(
            1, 
            $thread->subscriptions()->where('user_id', $userId )->count()
        );
    }

    /** @test */
    public function it_can_be_unsubscribed_from()
    {
        $thread = create('App\Thread');
    
        $thread->subscribe($userId = 1);

        $thread->unsubscribe($userId);
    
        $this->assertCount(0, $thread->subscriptions);
    }

    /** @test */
    public function it_knows_if_the_authenticated_user_is_subscribed_to_it()
    {
        $this->signIn();

        $thread = create('App\Thread');
        
        $this->assertFalse($thread->isSubscribedTo);

        $thread->subscribe();

        $this->assertTrue($thread->isSubscribedTo);
    
    }

    function a_thread_notifies_all_registered_subscribers_when_a_reply_is_added() 
    {
        Notification::fake();
        
        $this->signIn()
            ->thread
            ->subsribe()
            ->addReply([
                'body'  => 'Foobar',
                'user_id' => 999
            ]);

        Notification::assertSentTo(Auth::user, ThreadWasUpdated::class);

    }

    /** @test */
    function a_thread_can_check_if_the_authenticated_user_has_read_all_replies()
    {
        $this->signIn();

        $thread = create('App\Thread');

        tap(Auth::user(), function ($user) use ($thread) {

            $this->assertTrue($thread->hasUpdatesFor($user));

            $user->read($thread);
    
            $this->assertFalse($thread->hasUpdatesFor($user));
        });
    }
}
