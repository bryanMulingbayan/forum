<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function it_can_fetch_their_recent_reply()
    {
        $user = create('App\User');

        $reply = create('App\Reply', ['user_id' => $user->id]);

        $this->assertEquals($reply->id, $user->lastReply->id);
    }

    /** @test */
    function it_can_determine_avatar_path()
    {
        $user = create('App\User');

        $this->assertEquals('http://localhost/storage/avatars/default.png', $user->avatar());

        $user->avatar_path = 'storage/avatars/me.jpg';

        $this->assertEquals('http://localhost/storage/avatars/me.jpg', $user->avatar());

    }
}
